<?php
class User_model extends CI_Model {
	private static  $read_db=null;	
	/**
	 * 获取read_db的数据库连接
	 */
	private  function  read_db(){
		if(empty($read_db)){
			$read_db=$this->load->database('read_db', TRUE);
		}
		return $read_db;
	}
	
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    /**
     * @param string $uname
     * @result user
     */
    function getUserByName($uname){
    	try {
    		$query = $this->db->query('SELECT uid,upsw FROM ca_cusers WHERE uname='.$this->db->escape($uname).' LIMIT 1');
    		$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	 return $row;
    }
    /**
     * @param string $uname
     * @param string $upsw
     * @result user
     */
    function getUserByNameAndPsw($uname,$upsw){
    	try {
    		//$sql="SELECT * FROM ca_cusers WHERE uname=".$this->db->escape($uname)." AND upsw=".$this->db->escape($upsw)." LIMIT 1";
	    	$query = $this->db->query("SELECT * FROM ca_cusers WHERE uname=".$this->db->escape($uname)." AND upsw=".$this->db->escape($upsw)." LIMIT 1");
	    	$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error', $e->getMessage());
    	}
    	return $row;
    }
    
    /**
     * @param string uuid
     * @result user
     */
    function getUserByUuid($uuid){
    	try {
    		//$sql="SELECT * FROM ca_cusers WHERE uuid=".$this->db->escape($uuid)." LIMIT 1";
    		$query = $this->db->query("SELECT * FROM ca_cusers WHERE uid=".$this->db->escape($uuid)." LIMIT 1");
    		$row = $query->row_array();
    	} catch (Exception $e) {
    		log_message('error', $e->getMessage());
    	}
    	return $row;
    }
    
    /**
     * @param string uuid
     * @result user
     */
    function getUserByWxUuid($wxuuid){
    	try {
    		//$sql="SELECT * FROM ca_cusers WHERE uuid=".$this->db->escape($uuid)." LIMIT 1";
    		$query = $this->db->query("SELECT * FROM ca_cusers WHERE wxuuid=".$this->db->escape($wxuuid)." LIMIT 1");
    		$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error', $e->getMessage());
    	}
    	return $row;
    }
    
    /**
     * 
     * @param obj $user
     * @return 
     */
    function inertUser($user){
    	try {
         $result=$this->db->insert('ca_cusers', $user);
      } catch (Exception $e) {
      	log_message('error',  $e->getMessage());
      }
      return $result;
    }
     
    /**
     * 修改用户信息
     * @param array $user
     * @param int $uid
     * @return $result
     */
    function  updateUser($user,$uid){
    	try {
    	$where = "uid =". intval($uid);
    	$sql = $this->db->update_string('ca_cusers', $user, $where);
    	$result=$this->db->query($sql);
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    
    /**
     * 注册时写入token
     * @param obj $token
     * @return
     */
    function inertToken($token){
    	try {
    		$result=$this->db->insert('usertoken', $token);
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    /**
     * 修改token时间
     * @param array $token
     * @param int $uid
     * @return $result
     */
    function  updateToken($token,$uid){
    	try {
    	$where = "uid =". intval($uid);
    	$sql = $this->db->update_string('usertoken', $token, $where);
    	$result=$this->db->query($sql);
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    /**
     * @param string $uname
     * @param string $upsw
     * @result user
     */
    function getUserBlanceByUid($uid){
    	try {
    		$query = $this->db->query('SELECT balance FROM ca_cbalance WHERE uid='.intval($uid).' LIMIT 1');
    		$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $row;
    }
    
    /**
     * 扣减用户余额
     * @param int $flower 花的数量
     * @param int $uid
     * @return $result
     */
    function  updatePlusCblance($flower,$uid){
    	try {
    		$db=$this->db;
    		$sql ="update ca_cbalance set balance=balance-".intval($flower)." where uid=".$uid." AND balance>=".intval($flower);
    		$result=$db->query($sql);
    		$num=$db->affected_rows();
    		if ($result&&$num>0)
    		{
    			$result=true;
    		}else{
    			$result=false;
    		}
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    
    /**
     * 扣减用户钻石
     * @param int $flower 钻石的数量
     * @param int $uid
     * @return $result
     */
    function  updatePlusDiamond($flower,$uid){
    	try {
    		$db=$this->db;
    		$sql ="update ca_cbalance set diamond=diamond-".intval($flower)." where uid=".$uid." AND diamond>=".intval($flower);
    		$result=$db->query($sql);
    		$num=$db->affected_rows();
    		if ($result&&$num>0)
    		{
    		  $result=true;
    		}else{
    		  $result=false;
    		}
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    
    /**
     * 批量退钻石
     */
    function bulkReturnDiamond(){
    	try {
    		date_default_timezone_set('Asia/Shanghai');
    		$currdate=CommonTools::getcurrdate();
    		$starttime=$currdate['service_startime']-24*60*60;
    		$endtime=$currdate['service_endtime']-24*60*60;
    		$query = $this->db->query("select count(*) as num from event where estatus=1 and created>=$starttime and created<=$endtime");
    		$row = $query->row();
    		$num = (isset($row->num)&&!empty($row->num))?$row->num:0;
    		if($num){
    			$update = "update ca_cbalance, event set ca_cbalance.diamond=ca_cbalance.diamond+event.hbalance, event.estatus=0
						where ca_cbalance.uid=event.uid and created>=$starttime and created<=$endtime and event.estatus=1";
    			$res=$this->db->query($update);
    			$num = $this->db->affected_rows();
    			if($num>0){
    				$result = 1;
    			}else{
    				$result = 0;
    			}
    		}else{
    			$result = 3;
    		}
    	
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    /**
     * 批量退香槟
     * 
     */
    function bulkReturnBalance(){
    	try {
    		$query = $this->db->query("select count(*) as num from paidlist where status=0");
    		$row = $query->row();
    		$num = (isset($row->num)&&!empty($row->num))?$row->num:0;
    		if($num){
    			$update = "update ca_cbalance, paidlist, (SELECT  if((@i:=@i+1)>10,flower,0) as balance, uid, flower FROM (SELECT @i:=0) as i,paidlist WHERE status=0 ORDER BY flower DESC )tmp
						set ca_cbalance.balance=ca_cbalance.balance+tmp.balance, paidlist.status=1
						where ca_cbalance.uid=tmp.uid and paidlist.uid=tmp.uid";
    			$res=$this->db->query($update);
    			$num = $this->db->affected_rows();
    			if($num>0){
    				$result = 1;
    			}else{
    				$result = 0;
    			}
    		}else{
    			$result = 3;
    		}
    		
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    /**
     * 扣减用户余额
     * @param int $flower 花的数量
     * @param int $uid
     * @return $result
     * 没用
     */
    function  updatePlusCblance2($flower,$uid){
    	$this->db->trans_start();
    	try {
    		//$sql ="update ca_cbalance set balance=balance-".intval($flower)." where uid=".$uid." AND balance>=".intval($flower);
    		//$result=$this->db->query($sql);
    		
    		$data=array("balance"=>"balance-".intval($flower));
    		$where = array('uid' => $uid, 'balance>=' => intval($flower));
    		$this->db->where($where);
    		$result=$this->db->update('ca_cbalance', $data);
    		$this->db->trans_complete();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    
    /**
     * 增加用户余额
     * @param int $flower 花的数量
     * @param int $uid
     * @return $result
     */
    function  updateAddCblance($flower,$uid){
    	try {
    		$db=$this->db;
    		$sql ="update ca_cbalance set balance=balance+".intval($flower)." where uid=".$uid;
    		$result=$db->query($sql);
    		$num=$db->affected_rows();
    		if($num<=0){
    			$result=false;
    		}else{
    			$result=true;
    		}
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    
    /**
     * 增加用户钻石
     * @param int $flower 钻石的数量
     * @param int $uid
     * @return $result
     */
    function  updateAddDiamond($flower,$uid){
    	try {
    		$db=$this->db;
    		$sql ="update ca_cbalance set diamond=diamond+".intval($flower)." where uid=".$uid;
    		$result=$db->query($sql);
    		$num=$db->affected_rows();
    		if($num<=0){
    			$result=false;
    		}else{
    			$result=true;
    		}
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return  $result;
    }
    
    /**
     * 通过uid查找用户
     * @param int $uid
     * @result user array
     */
    function getUserByUid($uid){
    	try {
    		$query = $this->db->query('SELECT ca_cusers.*,ca_cbalance.balance FROM ca_cusers left join ca_cbalance on ca_cbalance.uid=ca_cusers.uid WHERE ca_cusers.uid='.intval($uid).' LIMIT 1');
    		$user = $query->row_array();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $user;
    }
    /**
     * 通过phone查找用户
     * @param string $phone
     * @result user array
     */
    function getUserByPhone($phone){
    	try {
    		$query = $this->db->query('SELECT ca_cusers.*,ca_cbalance.balance FROM ca_cusers left join ca_cbalance on ca_cbalance.uid=ca_cusers.uid WHERE ca_cusers.uname='.$this->db->escape($phone).' LIMIT 1');
    		$user = $query->row_array();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $user;
    }
    
    /**
     * 写入余额记录
     * @param array $ca_cbalance
     * @return $array
     */
    function inertca_cbalance($ca_cbalance){
    	try {
    		$result=$this->db->insert('ca_cbalance', $ca_cbalance);
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    /**
     * @param int uid
     * @result token
     */
    function getTokenByUid($uid){
    	try {
    		$query = $this->db->query('SELECT token FROM usertoken WHERE uid='.intval($uid).' LIMIT 1');
    		$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $row;
    }
    
    /**
     * @param int uid
     * @result ca_cbalance
     */
    function getca_cbalanceByUid($uid){
    	try {
    		$query = $this->db->query('SELECT uid,balance,diamond FROM ca_cbalance WHERE uid='.intval($uid).' LIMIT 1');
    		$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $row;
    }
    /**
     * 通过uid查找用户的微信号
     * @param int $uid
     * @result user array
     */
    function getUserWeixinByUid($uid){
    	try {
    		$query = $this->db->query('SELECT ca_cusers.weixin FROM ca_cusers  WHERE ca_cusers.uid='.intval($uid).' LIMIT 1');
    		$user = $query->row_array();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $user;
    }
    
    /**
     * 删除用户,测试使用
     */
    function delUser($uname){
    	try {
    		$deuser = $this->db->query('delete from ca_cusers where uname='.$uname);
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $deuser;
    }
    
    /**
     * 删除用户的全部信息
     */
    function delUserInfo($uname){
    	try {
    		$query = $this->db->query("SELECT uid FROM ca_cusers where uname=$uname");
    		$row = $query->row();
    		$uid = (isset($row->uid)&&!empty($row->uid))?$row->uid:0;
    		$deluser = $this->db->query('DELETE FROM sms_validate where phone_number='.$uname);
    		$delstore = $this->db->query("DELETE FROM store WHERE  event_id in (select event_id from event where event.uid=$uid)  or uid=$uid");
    		$delcevent = $this->db->query("DELETE FROM cevent WHERE  event_id in (select event_id from event where event.uid=$uid)  or uid=$uid");    		
    		$delexchange = $this->db->query("DELETE FROM exchange WHERE from_event in (select event_id from event where event.uid=$uid)  or from_uid=$uid or to_uid=$uid");
    		$delca_cbalance = $this->db->query("DELETE FROM ca_cbalance WHERE uid=$uid");
    		$delpaidlist = $this->db->query("DELETE FROM paidlist WHERE uid=$uid");
    		$delreport = $this->db->query("DELETE FROM report WHERE uid=$uid");
    		$delevent = $this->db->query("DELETE FROM event WHERE uid=$uid");
    		$deuser = $this->db->query('delete from ca_cusers where uname='.$uname);
    		return true;
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    		return false;
    	}
    }
    
    /**
     * 还原验证码
     */
    function updateVcode($vcode){
    	try {
    		$updatevcode = $this->db->query('update sms_validate set status=0 where validate_code='.$vcode);
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $updatevcode;
    }
    
    
    /**
     * 所有兴趣
     * @param int $uid
     * @result user array
     */
    function getAllInterest(){
    	try {
    		$query = $this->db->query('SELECT interest_id,interest_name,interest_desc FROM interest');
    		$result = $query->result_array();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    /**
     * 通过uid查找兴趣
     * @param int $uid
     * @result user array
     */
    function getInterestByUid($uid){
    	try {
    		$query = $this->db->query('SELECT ci.c_inst_id,i.interest_name,i.interest_desc FROM cinst ci left join interest i on ci.c_inst_id=i.interest_id WHERE ci.uid='.intval($uid));
    		$result = $query->result_array();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    
    /**
     * 通过uid查找标签
     * @param int $uid
     * @result user array
     */
    function getLabelByUid($uid){
    	try {
    		$query = $this->db->query('SELECT label_id,label_str FROM clabel WHERE uid='.intval($uid));
    		$result = $query->result_array();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    
    
    
    /**
     * 通过uid查找标签个数
     * @param int $uid
     * @result user array
     */
    function getLabelCountByUid($uid){
    	try {
    		$query = $this->db->query('SELECT count(label_id) as label_count FROM clabel WHERE uid='.intval($uid));
    		$result = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
    
    /**
    * 标签的新增
    * @param array $clabel=array('label_str'=>,'uid'=>)
    * @return bool
    */
   function insertLabel($clabel){
           try {
                   $result=$this->db->insert('clabel', $clabel);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return $result;
   }
   /**
    *标签的删除
    * @param int $label_id
    * @return bool
    */
   function deleteLabel($label_id){
           try {
                   $label_id=intval($label_id);
                   $sql="delete from clabel where label_id=".$label_id;
                   $result = $this->db->query($sql);
           } catch (Exception $e) {
                   log_message('error', $e->getMessage());
           }
           return $result;
   }
   
   /**
    * 新增兴趣项
    * @param int $uid
    * @param string $str
    * @return bool
    */
   function insertInterest($uid,$str){
           try {
                   if($this->deleteAllInterest($uid)){ //删除成功
                        $data=array();
                        $uid=intval($uid);
                        $str2=explode(",",$str);
                        foreach ($str2 as $k=>$v) {
                             $c_inst_id=intval($v);
                             $data2=array(
                                  'uid'=>$uid,
                                  'c_inst_id'=>$c_inst_id
                             );
                             array_push($data,$data2);
                        }
                        $result=$this->db->insert_batch('cinst', $data);
                   }else{
                        $result = false;
                   }
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return $result;
   }
   
   /**
    * 删除所有兴趣项
    * @param int $uid
    * @return bool
    */
   function deleteAllInterest($uid){
           try {
                   $uid=intval($uid);
                   $sql="delete from cinst where uid=".$uid;
                   $result = $this->db->query($sql);
           } catch (Exception $e) {
                   log_message('error', $e->getMessage());
           }
           return $result;
   }
   
   
   /**
     * 通过uid查找家乡
     * @param int $uid
     * @result user array
     */
    function getHometownByUid($uid){
    	try {
    		$query = $this->db->query('SELECT hometown_province,hometown_city,hometown_area FROM chometown WHERE uid= '.intval($uid));
    		$result = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
   /**
    * 添加家乡
    * @param array
    * @return bool
    */
   function insertHometown($data){
           try {
                   $result=$this->db->insert('chometown', $data);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return $result;
   }
   
   /**
    * 更新家乡
    * @param array
    * @param int $uid
    * @return bool
    */
   function updateHometown($uid,$data){
           try {
                   $where="uid=".intval($uid);
                   $result=$this->db->update('chometown', $data, $where);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return  $result;
   }   
   
   /**
    * 是否有家乡
    * @param int $uid
    * @return bool
    */
   function isHometown($uid,$data){
           try {
                   if(!$this->getHometownByUid($uid)){
                           $data['uid']=$uid;
                           $result = $this->insertHometown($data);
                   }else{
                           $result = $this->updateHometown($uid,$data);
                   }
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return  $result;
   }
   
   
    /**
     * 通过uid查找个性签名
     * @param int $uid
     * @result user array
     */
    function getSignByUid($uid,$data){
    	try {
    		$query = $this->db->query('SELECT sign_id,sign_str FROM csign WHERE uid='.intval($uid));
    		$result = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
   /**
    * 添加签名
    * @param array $data=array（'uid'=>,sign_str'=>）
    * @return bool
    */
    function insertSign($data){
        try {
                $result=$this->db->insert('csign', $data);
        } catch (Exception $e) {
                log_message('error',  $e->getMessage());
        }
        return $result;
    }   
   
   /**
    * 更新签名
    * @param array $data=array（'sign_str'=>）
    * @param int $uid
    * @return bool
    */
   function updateSign($data,$uid){
        try {
                $where="uid=".intval($uid);
                $result=$this->db->update('csign', $data, $where);
        } catch (Exception $e) {
                log_message('error',  $e->getMessage());
        }
        return  $result;
   }
   
   
   /**
    * 是否有个性签名 （有的话返回true）
    * @param int $uid
    * @return bool
    */
   function isSign($uid,$data){
        try {
                 if(!$this->getSignByUid($uid,$data)){
                        $data['uid'] = $uid;
                        $result = $this->insertSign($data);
                 }else{
                        $result = $this->updateSign($data,$uid);
                 }
        } catch (Exception $e) {
                log_message('error',  $e->getMessage());
        }
        return  $result;
   }
   
   /**
     * 查询血型
     * @param string $uname
     * @result user
     */
    function getBloodtypeByUid($uid){
    	try {
    		$query = $this->db->query('SELECT bloodtype_name FROM cbloodtype WHERE uid='.intval($uid));
    		$row = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	 return $row;
    }
    
   /**
    * 更新血型
    * @param
    * @param int $uid
    * @return bool
    */
   function updateBloodtype($data,$uid){
           try {
                   $where="uid=".intval($uid);
                   $result=$this->db->update('cbloodtype', $data, $where);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return  $result;
   }
   
   /**
    * 添加血型
    * @param array 
    * @return bool
    */
   function insertBloodtype($data){
           try {
                   $result=$this->db->insert('cbloodtype', $data);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return $result;
   }   
   
   /**
    * 是否有血型 （有的话返回true）
    * @param int $uid
    * @return bool
    */
   function isBloodtype($uid,$data){
        try {
            if(!$this->getBloodtypeByUid($uid)){
                 $data['uid'] = $uid;
                 $result = $this->insertBloodtype($data);
            }else{
                 $result = $this->updateBloodtype($data,$uid);
            }
        } catch (Exception $e) {
                log_message('error',  $e->getMessage());
        }
        return  $result;
   }
   
    
    /**
     * 通过uid查找星座
     * @param int $uid
     * @result user array
     */
    function getConstellationByUid($uid){
    	try {
    		$query = $this->db->query('SELECT constellation_id,constellation_name FROM cconstellation WHERE uid= '.intval($uid));
    		$result = $query->row();
    	} catch (Exception $e) {
    		log_message('error',  $e->getMessage());
    	}
    	return $result;
    }
    
   /**
    * 添加星座
    * @param array
    * @return bool
    */
   function insertConstellation($data){
           try {
                   $result=$this->db->insert('cconstellation', $data);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return $result;
   }
   
   /**
    * 更新星座
    * @param array
    * @param int $uid
    * @return bool
    */
   function updateConstellation($uid,$data){
           try {
                   $where="uid=".intval($uid);
                   $result=$this->db->update('cconstellation', $data, $where);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return  $result;
   }   
   
   /**
    * 是否有星座
    * @param int $uid
    * @return bool
    */
   function isConstellation($uid,$data){
           try {
                   if(!$this->getConstellationByUid($uid)){
                           $data['uid']=$uid;
                           $result = $this->insertConstellation($data);
                   }else{
                           $result = $this->updateConstellation($uid,$data);
                   }
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return  $result;
   }
   
   
   /**
    * 添加赠送香槟、钻石记录
    * @param array 
    * @return bool
    */
   function insertInvitecode_send_detail($data){
           try {
                   $result=$this->db->insert('cinvitecode_send_detail', $data);
           } catch (Exception $e) {
                   log_message('error',  $e->getMessage());
           }
           return $result;
   } 
   
   
   /**
     * 插入用户信息并返回uid
     * @param obj $user
     * @return 
     */
    function inertUserReturnUid($user){
        $uid=0;
    	try {
          $db=$this->db;
          $result=$db->insert('ca_cusers', $user);
         if($result){
            $uid=$db->insert_id();
         }
      } catch (Exception $e) {
      	log_message('error',  $e->getMessage());
      }
      return $uid;
    }

	/**
	 * 添加星座
	 * @param array
	 * @return bool
	 */
	function inertCbalance($data){
		try {
			$result=$this->db->insert('ca_cbalance', $data);
		} catch (Exception $e) {
			log_message('error',  $e->getMessage());
		}
		return $result;
	}

	/**
	 * 获取用户余额
	 * @param $uid
	 * @return mixed
	 */
//	function getCbalanceByUid($uid){
//		try {
//			$result=$this->db->query('select balance from ca_cbalance where uid='.intval($uid));
//		} catch (Exception $e) {
//			log_message('error',  $e->getMessage());
//		}
//		return $result;
//	}
    
}