<?php
namespace Config;
use \GatewayWorker\Lib\Db as NDB;
/**
 * mysql配置
 * @author walkor
 */
class Db
{
    /**
     * 数据库的一个实例配置，则使用时像下面这样使用
     * $user_array = Db::instance('db1')->select('name,age')->from('users')->where('age>12')->query();
     * 等价于
     * $user_array = Db::instance('db1')->query('SELECT `name`,`age` FROM `users` WHERE `age`>12');
     * @var array
     */
    public static $db = array(
        'host'    => 'localhost',
        'port'    => 3306,
        'user'    => 'root',
        'password' => '',
        'dbname'  => 'acaradvertisting',
        'charset'    => 'utf8',
    );

    /**
     * 获取设备信息
     */
    function getDeviceInfo($data){
        try {
            if(empty($data)){
                return false;
            }
            $result = NDB::instance('db')->row("SELECT * FROM ca_device WHERE deviceNum='$data' order by id desc limit 1");
        } catch (Exception $e) {
            log_message('error',  $e->getMessage());
        }
        return $result;
    }

    /**
     * 根据设备号获取用户uid
     */
    function getUidByDeviceId($deviceId){
        try {
            if(empty($deviceId)){
                return false;
            }
            $result = $this->dbWorker->row("SELECT * FROM ca_device WHERE deviceNum='$deviceId' limit 1");
        } catch (Exception $e) {
            log_message('error',  $e->getMessage());
        }
        return $result;
    }

    /**
     * 经纬度入库
     */
    function insertLongLat($data){
        try {
            $time = time();
            $insertData = array(
                'uid' => $data['uid'],
                'device_id' => $data['device_id'],
                'longitude' => $data['longitude'],
                'latitude' => $data['latitude'],
                'create_time' => $time
            );
            $result = $this->dbWorker->insert('ca_long_lat')->cols($insertData)->query();
        } catch (Exception $e) {
            log_message('error',  $e->getMessage());
        }
        return $result;
    }

}